<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1"/>
    
    <title>Prequel - Dungeon Crawl Stone Soup</title>

    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="fragment" content="!">

    <link rel="shortcut icon" href="img/favicon.png" type="image/png"/>

    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="src/bower_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="src/css/font.css">
    <link rel="stylesheet" type="text/less" href="src/css/prequel.less.css">
    <link rel="stylesheet" type="text/css" href="src/css/icomoon.css">

    <script id="full-game-template" type="text/template">
    </script>

    <script id="games-template" type="text/template">
        
    </script>

    <script id="game-template" type="text/template">
        <div class="key-value col-md-2">
            <span class="key">Score</span>
            <span class="value"><%= sc %></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">Player</span>
            <span class="value"><%= name %></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">Character</span>
            <span class="value"><%= char %></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">God</span>
            <span class="value"><i class="code god <%= god.toLowerCase() %>"></i></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">Title</span>
            <span class="value"><%= title %></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">XL</span>
            <span class="value"><%= xl %></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">Turns</span>
            <span class="value"><%= turn %></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">Duration</span>
            <span class="value"><%= dur %></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">Runes</span>
            <span class="value"><%= nrune %></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">Date</span>
            <span class="value"></span>
        </div>
        <div class="key-value col-md-2">
            <span class="key">Version</span>
            <span class="value"><%= cv %></span>
        </div>
    </script>

    <script id="no-game-template" type="text/template">
        No game to show...
    </script>

    <script id="search-template" type="text/template">
        <input class="form-control search-input" type="text" name="q" placeholder="Type !lg/!lm command...">
    </script>

</head>

<body>

    <div class="prequel">
        
        <div id="search">
        </div>

        <div id="result">
            
        </div>

    </div>
    
    <script type="text/javascript" src="src/bower_components/less/dist/less.js"></script>
    <script type="text/javascript" src="src/bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="src/bower_components/lodash/lodash.js"></script>
    <script type="text/javascript" src="src/bower_components/moment/moment.js"></script>
    <script type="text/javascript" src="src/bower_components/backbone/backbone.js"></script>
    <script type="text/javascript" src="src/bower_components/backbone.wreqr/lib/backbone.wreqr.js"></script>
    <script type="text/javascript" src="src/bower_components/backbone.babysitter/lib/backbone.babysitter.js"></script>
    <script type="text/javascript" src="src/bower_components/marionette/lib/backbone.marionette.js"></script>

    <script type="text/javascript" src="src/js/Module/Crawl/Model/Game.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/Model/Milestone.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/Collection/Games.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/Collection/Milestones.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/Controller/SearchController.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/Controller/GameController.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/Controller/MilestoneController.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/Router/SearchRouter.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/Router/GameRouter.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/Router/MilestoneRouter.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/View/SearchView.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/View/FullGameView.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/View/GameView.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/View/NoGameView.js"></script>
    <script type="text/javascript" src="src/js/Module/Crawl/View/GamesView.js"></script>

    <script type="text/javascript" src="src/js/app.js"></script>

</body>

</html>
