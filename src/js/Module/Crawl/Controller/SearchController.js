SearchController = Backbone.Marionette.Controller.extend({

    initialize: function () {
        Prequel.vent.on('initial', this.showInitial.bind(this));
    },

    showInitial: function () {
        var searchView = new SearchView();

        Prequel.searchRegion.reset()
        Prequel.searchRegion.show(searchView);
    }

});
