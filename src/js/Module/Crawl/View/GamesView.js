GamesView = Backbone.Marionette.CompositeView.extend({

    tagName: 'div',

    className: 'games',
    template: '#games-template',

    childView: GameView,
    emptyView: NoGameView,

    postId: null,

    events: {
    },

    onDomRefresh: function () {
    },

    initialize: function (options) {
        //Intaglio.vent.on('gameError', this.showGameError.bind(this));
    }

});